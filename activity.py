# Activity S02
# Python Control Structures

# 1.

while True:
    try:
        year = int(input("Please input a year "))
        print(year)

        if year <= 0:
            print("No zero or negative values")

        elif year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
            print(year, "is a leap year")
            break
        else:
            print(year, "is not a leap year")
            break

    except ValueError:
            print("Strings are not allowed for inputs")

# 2.

row = int(input("Enter number of rows "))
print(row)

column = int(input("Enter number of columns "))
print(column)

i = 0
while i < row :
    print(column * "*")
    i += 1
